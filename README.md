# Robot Simulator
Processing project to simulate robot from a top down perspective. Complete with distance calculations from a path as well as distance from a set of "barrels." <br />
![](Simulator.png)
