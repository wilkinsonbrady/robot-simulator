robot r;
path p;

PVector A,B,C;
float pathPointXY[] = {500, 200, 400, 450, 130, 450, 80,100,250,50};
float barrelPointXY[] = {440, 220, 460, 450, 100, 450, 150,100,280,20};

void setup(){
  size(600, 650);
  ArrayList<PVector> pathPoints = new ArrayList<PVector>();
  ArrayList<PVector> barrelPoints = new ArrayList<PVector>();
  r = new robot(width/2, height/2);
  
  A = new PVector (100,0);
  B = new PVector (50,100);
  C = new PVector (250,130);
  
  for(int i = 0; i < pathPointXY.length; i +=2){
    PVector Point = new PVector(pathPointXY[i], pathPointXY[i+1]);
    pathPoints.add(Point);
  }
  for(int i = 0; i < barrelPointXY.length; i +=2){
    PVector Point = new PVector(barrelPointXY[i], barrelPointXY[i+1]);
    barrelPoints.add(Point);
  }
  p = new path(pathPoints, barrelPoints);
}

void draw(){
  background(150); 
  
  p.drawPath();
  
  p.distance(A,B,C);
  
  p.closePath(r.location);
  p.closeBarrel(r.location);
  
  r.run();
  
  fill(130,255,40);
  text("Controls:", 3-2, height - 80);
  text("I - Left Wheel Speed F", 3, height - 65);
  text("K - Left Wheel Speed R", 3 + 150, height - 65);
  text("R - Randomize path", 3 + 400, height - 65);
  text("O - Right Wheel Speed F", 3 - 2, height - 50);
  text("L - Right Wheel Speed R", 3 + 150, height - 50);
  text("Y - Match Wheel Speeds", 3 - 1, height - 35);
  text("H - Stop Wheels", 3 + 150, height - 35);
  text("Space - Debug View, green normal lines = on the line, blue = not on the line,  red path = closest path", 3 - 1, height - 20);
  text("Red Line on bot = velocity vector     Blue Line = Direction Vector", 3 - 1, height - 5);
  fill(255);
}

void keyReleased(){    //Debugging
    //if(key == 'q'){
    //  r.rotate(-0.01);
    //}
    //if(key == 'e'){
    //  r.rotate(0.01);
    //}if(key == 's'){
    //  r.addForce(new PVector (0, 0.01));
    //}if(key == 'w'){
    //  r.addForce(new PVector (0, -0.01));
    //}if(key == 'a'){
    //  r.addForce(new PVector (-0.001, 0));
    //}if(key == 'd'){
    //  r.addForce(new PVector (0.001, 0));
    //}
    ArrayList<PVector> pathPoints = new ArrayList<PVector>();
    
    if(key == 'o'){
      r.wheel(0.002, 0);
    }if(key == 'i'){
      r.wheel(0,0.002);
    }if(key == 'k'){
      r.wheel(0,-0.002);
    }if(key == 'l'){
      r.wheel(-0.002, 0);
    }if(key =='y'){
      if(r.RWheel < r.LWheel){
        r.LWheel = r.RWheel;
      } else{
        r.RWheel = r.LWheel;
      }
    }if(key =='h'){
      r.LWheel = r.RWheel=0;
    }if(key == ' '){
      p.debugOnOff(!p.debug);
    }if(key=='r'){
      for(int i = 0; i < pathPointXY.length; i +=2){
        PVector Point = new PVector(pathPointXY[i] + random(-20,20), pathPointXY[i+1] + random(-20,20));
        pathPoints.add(Point);
      }
      p.points = pathPoints;
    }
}
