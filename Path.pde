class path{
  ArrayList<PVector> points;
  ArrayList<PVector> barrelPoints;
  boolean debug;
  
  path(ArrayList<PVector> pathPoints,ArrayList<PVector> barPoints){
    points = pathPoints;
    barrelPoints = barPoints;
    debug = false;
  }
  
  void drawPath(){
    
    for(int i=1; i< points.size(); i++){
      stroke(175);
      strokeWeight(30*2);
      line(points.get(i-1).x,points.get(i-1).y,points.get(i).x,points.get(i).y);
      if (i == points.size()-1){
        line(points.get(0).x,points.get(0).y,points.get(i).x,points.get(i).y);
      }
    }
    
    for(int i=1; i< points.size(); i++){
      stroke(0);
      strokeWeight(2);
      line(points.get(i-1).x,points.get(i-1).y,points.get(i).x,points.get(i).y);
      if (i == points.size()-1){
        line(points.get(0).x,points.get(0).y,points.get(i).x,points.get(i).y);
      }
    }    
    
    fill(250,140,0);
    for(int i=0; i< barrelPoints.size(); i++){
      strokeWeight(2);
      ellipse(barrelPoints.get(i).x, barrelPoints.get(i).y, 30,30);
    }
    fill(255);
  }
  
  ArrayList<PVector> closePath(PVector loc){
    ArrayList<PVector> pathList = points;
    float shortDis = 999999;
    PVector point1 = new PVector(0,0);
    PVector point2 = new PVector(0,0);
    PVector normPoint = new PVector(0,0);
    boolean onLine;
    ArrayList<PVector> lineSeg = new ArrayList<PVector>();
    
    for (int i = 1; i < pathList.size(); i++){
      onLine = false;
      normPoint = distance(pathList.get(i-1),pathList.get(i),loc);
     
     
      //Debug
      if(debug){
        stroke(0);
        ellipse(int(pathList.get(i-1).x),int(pathList.get(i-1).y),5,5);
        ellipse(int(pathList.get(i).x),int(pathList.get(i).y),5,5);
      }
      //
      
      
      if(pathList.get(i-1).x < pathList.get(i).x){
        if(normPoint.x >= pathList.get(i-1).x && normPoint.x <= pathList.get(i).x){
          onLine = true;
        }
      }else if(pathList.get(i-1).x > pathList.get(i).x){
        if(normPoint.x < pathList.get(i-1).x && normPoint.x > pathList.get(i).x){
          onLine = true;
        }
      }else{
       if(pathList.get(i-1).y < pathList.get(i).y){
        if(normPoint.y >= pathList.get(i-1).y && normPoint.y <= pathList.get(i).y){
            onLine = true;
          }
        }else{
          if(normPoint.y <= pathList.get(i-1).y && normPoint.y >= pathList.get(i).y){
            onLine = true;
          }
        } 
      }
      
      
      //Debug
      //println("-----------------");
      //println(i);
      //println(normPoint.x);
      //println(pathList.get(i-1).x);
      //println(pathList.get(i).x);
      
      //Debug
      if(debug){
        if(onLine){
         stroke(0,255,0);
         line(normPoint.x,normPoint.y,loc.x,loc.y);
        }else{
         stroke(0,0,255);
         line(normPoint.x,normPoint.y,loc.x,loc.y);
         ellipse(normPoint.x,normPoint.y,10,10);
        }
      }
      
      
      normPoint.sub(loc);
      if(shortDis>normPoint.mag() && onLine){
        shortDis = normPoint.mag();
        point1 = pathList.get(i-1);
        point2 = pathList.get(i);
      }
      
      
      if(i == pathList.size() - 1){
        onLine = false;
        normPoint = distance(pathList.get(0),pathList.get(i),loc);
        
        //line(normPoint.x,normPoint.y,loc.x,loc.y);
        
        if(pathList.get(i).x < pathList.get(0).x){
          if(normPoint.x >= pathList.get(i).x && normPoint.x <= pathList.get(0).x){
            onLine = true;
          }
        }else if(pathList.get(i).x > pathList.get(0).x){
          if(normPoint.x <= pathList.get(i).x && normPoint.x >= pathList.get(0).x){
            onLine = true;
          }
        }else{
          if(pathList.get(i).y < pathList.get(0).y){
            if(normPoint.y >= pathList.get(i).y && normPoint.y <= pathList.get(0).y){
              onLine = true;
            }
          }else{
            if(normPoint.y <= pathList.get(i).y && normPoint.y >= pathList.get(0).y){
              onLine = true;
            }
          }
        }
        
        if(debug){
          if(onLine){
           stroke(0,255,0);
           line(normPoint.x,normPoint.y,loc.x,loc.y);
          }else{
           stroke(0,0,255);
           line(normPoint.x,normPoint.y,loc.x,loc.y);
           ellipse(normPoint.x,normPoint.y,10,10);
          }
        }
        
        normPoint.sub(loc);
        if(shortDis>normPoint.mag() && onLine){
          shortDis = normPoint.mag();
          point1 = pathList.get(i);
          point2 = pathList.get(0);
        }  
      }
        
    }
    
   if(debug){
      stroke(255,0,0);
      line(point1.x,point1.y, point2.x,point2.y);
      stroke(0);
    }
    
    return lineSeg;
  }
  
  PVector distance(PVector x1, PVector x2, PVector y){
    //PVector out = y; 
    //out.normalize();
    //out.mult(x.dot(out));
    
    //line(x.x,x.y,100,100);
    //line(y.x,y.y,100,100);
    //line(y.x,y.y,out.x,out.y);
    
    PVector ap = PVector.sub(y, x1);
    PVector ab = PVector.sub(x2, x1);
    ab.normalize(); // Normalize the line
    ab.mult(ap.dot(ab));
    PVector normalPoint = PVector.add(x1, ab);
    
    //translate(width/2,height/2);
    //line(x1.x,x1.y,x2.x,x2.y);
    //line(x1.x,x1.y,y.x,y.y);
    //ellipse(normalPoint.x,normalPoint.y,10,10);
    
    //PVector distVec = PVector.sub(normalPoint,y);
    
    return normalPoint;
  }
  
  float objDist(PVector a, PVector b){
    PVector distVec = PVector.sub(a,b);
    return distVec.mag();
  }
  
  void debugOnOff(boolean onOff){
    debug = onOff;
  }
  
  PVector closeBarrel(PVector loc){
    float dist = 9999;
    float record = 9999;
    int closeI = 0;
    for(int i = 0; i < barrelPoints.size(); i++){
       dist = objDist(loc, barrelPoints.get(i));
       stroke(255,0,255);
       if(dist < record){
         record = dist;
         closeI = i;
         println("hello");
       }
       line(barrelPoints.get(i).x, barrelPoints.get(i).y, loc.x, loc.y);
     }
     stroke(0);
     fill(0,255,0);
     ellipse(barrelPoints.get(closeI).x,barrelPoints.get(closeI).y, 30,30);
     fill(255);
     
     return barrelPoints.get(closeI);
  }
  
  
}
