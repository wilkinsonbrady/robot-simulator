class robot{
 float angleLimit = 0.05; //Fastest anglular velocity
 float speedLimit = 1; //Fastest linear velocity
 float wheelSpeedLimit = 0.01; //Max Wheel speed
 float angleDamp = 0.96; //% of loss of angular velocity each calculation
 float speedDamp = 0.98; //% of loss of linear velocity each calculation
  
  
 PVector location;
 PVector velocity;
 PVector acceleration;
 PVector direction;
 
 float LWheel; //L wheel speed
 float RWheel; //R wheel speed
 
 
 //Initilize
 float angleA = 0; // angular A
 float angleV = 0; //angular V
 float angle = 0;
 float wheelSpeed = 0; //forward V
 
 
 float vehicleWidth=50;
 
 
 
 robot(float x, float y){
   //initliaze vars
   location = new PVector(x,y);
   velocity = new PVector(0,0);
   acceleration = new PVector(0,0);
   
   LWheel = 0;
   RWheel = 0;
   
   direction = new PVector (1,0);
 }

  
  
 ////////////////////////////////////////RUNTIME/////////////////////////////////////
 void wheel(float a, float b){ //change wheel speed, a is R wheel, b is L wheel  
   RWheel+=a;
   RWheel = constrain(RWheel, -wheelSpeedLimit, wheelSpeedLimit); 
   LWheel+=b;
   LWheel = constrain(LWheel, -wheelSpeedLimit, wheelSpeedLimit);
  }
 
 
 void run(){
  this.update();
  this.edges();
  this.display();
 }
 
 
 ////////////////////////////////////////DEBUG/////////////////////////////////////
 void addForce(PVector force){ //Debug
   PVector f = new PVector(force.x, force.y);
   acceleration = acceleration.add(f);
 }
 
 
 void rotate(float a){ //Debug
   angleA = a;
 }
 

 
 
 ////////////////////////////////////////CALCULATIONS/////////////////////////////////////
 private void update(){   
   //Differential steering
   angleA = (LWheel-RWheel); //Calculate angular accelleration
   angleV += angleA;
   angleV = constrain(angleV, -angleLimit, angleLimit); //Keep angular velocity from going insane
   angle += angleV;
   angleA = 0; //reset angular velocity
   
   //direction vector
   direction.x = cos(angle);
   direction.y = sin(angle);   
   
   //x y movement
   wheelSpeed = LWheel+RWheel; //forward acceleration
   velocity = velocity.add(direction.mult(wheelSpeed)); //Points the velocity vector in the same direction as the direction vector
   //velocity = velocity.add(direction.mult(acceleration.mag()));
   velocity.limit(speedLimit); //keep velocity from going insane
   acceleration.mult(0); //reset acceleration
   
   //Update Location
   location = location.add(velocity);
   
   //Dampening
   angleV *= angleDamp;
   velocity.mult(speedDamp);
 }
 
 
 private void edges(){ //Wraparound
   if(location.x >= width){
     location.x = 1;
   }
   if(location.x <= 0){
     location.x = width;
   }
   
   if(location.y <= 0){
     location.y = height-1;
   }
   if(location.y >= height){
     location.y = 1;
   }
 }
 
 
 private void display(){
   
   float vScale = 50;
   float wScale = 10000;
   
   push();
   translate(location.x,location.y);
   
   //display robot
   ellipseMode(CENTER);
   stroke(0);
   ellipse(0, 0, vehicleWidth,vehicleWidth);
   stroke(0,0,255);
   line(0,0, vehicleWidth*cos(angle), vehicleWidth*sin(angle));
   
   //Velocity Lines/Wheel Speeds
   stroke(255,0,0);
   line(0,0, velocity.x*vScale, velocity.y*vScale); //Velocity
   if(abs(RWheel) != abs(LWheel)){
    stroke(0,205,120); 
   } else {
    stroke(0,255,0);
   }
   line(vehicleWidth,0,vehicleWidth,-RWheel*wScale); //Wheel Speed R
   line(-vehicleWidth,0,-vehicleWidth,-LWheel*wScale); //Wheel Speed L
   stroke(0);
   line (-vehicleWidth-5, 0, -vehicleWidth+5, 0);
   line (vehicleWidth+5, 0, vehicleWidth-5, 0);
   pop();
 }
 
}
